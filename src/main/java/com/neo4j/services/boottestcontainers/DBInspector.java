package com.neo4j.services.boottestcontainers;

import org.neo4j.driver.Driver;
import org.springframework.stereotype.Service;

/**
 * A stupid little test service to connect to the database and return some stats.
 */
@Service
public class DBInspector {

    private final Driver driver;

    public DBInspector(Driver driver) {
        this.driver = driver;
    }

    public int getNodeCount() {

        try (var session = driver.session()){

            return session.executeRead(tx -> {
                var result = tx.run("MATCH (n) RETURN count(n) AS cnt");
                return result.single().get("cnt").asInt();
            }
            );
        }
    }
}
