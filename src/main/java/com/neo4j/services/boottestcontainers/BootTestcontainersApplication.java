package com.neo4j.services.boottestcontainers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootTestcontainersApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootTestcontainersApplication.class, args);
    }

}
