package com.neo4j.services.boottestcontainers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.TestConfiguration;

@TestConfiguration(proxyBeanMethods = false)
public class TestBootTestcontainersApplication {

    public static void main(String[] args) {
        SpringApplication.from(BootTestcontainersApplication::main).with(TestBootTestcontainersApplication.class).run(args);
    }

}
